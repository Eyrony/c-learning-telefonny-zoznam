// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#pragma once
#include "stdafx.h"
#include "ViewMenuElements.h"
#include <string>
#include <stdio.h>
#include <thread>

ViewMenuElements::ViewMenuElements()
{
}

ViewMenuElements::~ViewMenuElements()
{
}

std::string ViewMenuElements::ViewAddContact(const short& DataInputTick)
{
	std::string EnteredData;
	system("CLS");
	//
	//Data input
	
	switch (DataInputTick)
	{
	case 0: {
		
		std::cout << "Enter following: \nName: ";
		break;
	}
	case 1: {
		while (1)
		{
			std::cout << "\nNumber: ";
			std::getline(std::cin, EnteredData);
			if (EnteredData.find_first_not_of("0123456789") != std::string::npos)
			{
				std::cout << "\nError: Phone number can only consisst of numbers!";
				continue;
			}
			else return EnteredData;
		}
	}
	case 2: {
		std::cout << "\nAddress: ";
		break;
	}
	}
	std::getline(std::cin, EnteredData);
	return EnteredData;
}

void ViewMenuElements::ErrorMessages(const int& RecievedError) {
	system("CLS");
	switch (RecievedError) {
	case 1: {std::cout << "\nError: File could not be loaded.\n"; break; }
	case 2: {std::cout << "\nError: Incorrect selection.\n"; break; }
	case 3: {std::cout << "\nError: Unable to save new data.\n"; break; }
	case 4: {std::cout << "\nNew data saved successfully.\n"; break;}
	case 5: {std::cout << "\nError: Unable to save new data. Contact already exists.\n"; break; }
	case 6: {std::cout << "\nError: Incorrect selection or data entered was incorrect.\n"; break; }
	case 7: {std::cout << "\nError: Failed to save new data. Retry save?Y/N\n"; break; }
	case 8: {std::cout << "\nError: Option unavailable. No more contacts to load.\n"; break; }
	case 9: {std::cout << "\nError: No contact found matching crietria.\n"; break; }
	default: break;
	}
	std::this_thread::sleep_for(std::chrono::seconds(1));
	return;
}

char ViewMenuElements::ViewSearchAndEdit()
{
	system("CLS");
	std::cout << "Search by: \n1. Name\t2. Address\n3. Number\t 0. Exit\nOption: \n";
	char SearchSelection= _getch();
		switch (SearchSelection)
		{
		case '1': 
		{
			std::cout << "Please enter name: ";
			break;
		}
		case '2': 
		{
			std::cout << "Please enter address: ";
			break;
		}
		case '3':
		{
			std::cout << "Please enter number: ";
			break;
		}
		case '0': return 'q';
		default:
		{
			std::cout << "Error: Incorrect selection.";
			break;
		}
		}
	return SearchSelection;
}

char ViewMenuElements::DisplayOutput(std::string& OutputName, std::string& OutputNumber, std::string& OutputAddress)
{
	system("CLS");
	while (1)
	{
		std::cout << OutputName << '\n' << OutputNumber << '\n' << OutputAddress << '\n';
		std::cout << "N: Next; B: Back; E: Edit; D: Delete; W: New search; Q: Quit.\n";
		char NextBackEditQuit = _getch();
		switch (NextBackEditQuit)
		{
		case 'w':
		case 'W':
		case 'b':
		case 'B':
		case 'n':
		case 'N':
		case 'e':
		case 'E':
		case 'q':
		case 'Q':
		case 'd':
		case 'D':return NextBackEditQuit;
		default: {std::cout << "No such option.\n"; break; }
		}
	}
}