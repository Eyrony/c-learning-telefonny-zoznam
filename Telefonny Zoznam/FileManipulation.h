#pragma once
#include <fstream>
#include "ContactDataModule.h"

class FileManipulation:public ContactDataModule
{
private:

	std::string GETSTRING(std::ifstream&);

public:
		
	void LoadFile();
	FileManipulation(char);
	void FileCreation();
	~FileManipulation();
		
};