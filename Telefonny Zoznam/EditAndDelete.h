#pragma once
#include "FileManipulation.h"
#include "ViewMenuElements.h"

void MenuDecisionCall();

class EditAndDelete:public ViewMenuElements, public FileManipulation
{

private:

public:

	EditAndDelete(char OptionCatch);
	ContactData DataAcquisition();
	void IsNumber(std::string& InputCheck);
	~EditAndDelete();

};