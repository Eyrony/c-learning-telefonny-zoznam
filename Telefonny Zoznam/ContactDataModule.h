#pragma once
#include <string>
#include <vector>
#include <algorithm>
#include <memory>
#include <list>

struct ContactData {
	std::string Name;
	std::string Number;
	std::string Address;
	ContactData(std::string NameIN, std::string AddressIN, std::string NumberIN) : Name(NameIN), Number(NumberIN), Address(AddressIN) {};
	bool operator<(const ContactData& Compare2) const { return Name < Compare2.Name; };
};



class ContactDataModule
{
private:

	char NextOption;
	std::list<size_t> Found_Mach;
	std::vector<ContactData> DataVector;
	unsigned Cycle_Check;

public:
	
	ContactDataModule(char Optioncatch);
	~ContactDataModule();

	char* const Option_Ptr = &NextOption;
	bool Size_check() { return Found_Mach.size(); };
	bool FindSearchedData(const char&,const std::string&);
	bool AddContact(ContactData&);
	std::string Retrieve_Contact(const short&);
	bool Contact_Manipulation();
	auto Save_Contact_Vector() { DataVector.shrink_to_fit(); std::sort(DataVector.begin(), DataVector.end()); return DataVector; };

};