
#pragma once
#include "stdafx.h"
#include "FileManipulation.h"
#include <stdio.h>

FileManipulation::FileManipulation(char Optioncatch) :ContactDataModule(Optioncatch)
{
	FileManipulation::LoadFile();
}

FileManipulation::~FileManipulation()
{
	FileManipulation::FileCreation();
}

void FileManipulation::LoadFile()
{	
	std::ifstream LoadedContactFile("Contacts.txt", std::ios::in | std::ios::app);
	std::string TempString;
	if (LoadedContactFile.is_open())
		{
			while (!LoadedContactFile.eof()) {
				std::getline(LoadedContactFile, TempString);
				if (TempString.find("contact") == std::string::npos) {
					ContactData TempContact(
						TempString,
						GETSTRING(LoadedContactFile),
						GETSTRING(LoadedContactFile));
					AddContact(TempContact);
				}
			}
	}
	LoadedContactFile.close();
}

std::string FileManipulation::GETSTRING(std::ifstream& PassedFile)
{
	std::string LocalStr;
	std::getline(PassedFile,LocalStr);
	return LocalStr;
}


void FileManipulation::FileCreation()
{
	std::ofstream OutputFile("ContactsNew.txt", std::ios::out | std::ios::app);
	auto Contacts_To_Save=ContactDataModule::Save_Contact_Vector();
	Contacts_To_Save.shrink_to_fit();
	if (OutputFile.is_open())
	{
		unsigned int ContactCount = 0;
		for (auto WriteContact : Contacts_To_Save)
		{
			OutputFile << "contact " << ContactCount << '\n'<< WriteContact.Name << '\n' << WriteContact.Number << '\n'<< WriteContact.Address << '\n';
			ContactCount++;
		}
		OutputFile.close();
		remove("Contacts.txt");
		rename("ContactsNew.txt", "Contacts.txt");
	}
}