#pragma once
#include <iostream>
#include <conio.h>



class ViewMenuElements {
private:
	
	

public:

	ViewMenuElements();
	void ViewInitialMenu() { std::cout << "Choose next step:\n1. Insert contact\t2. Search, edit and delete contact\t0. Exit: \n"; return; };
	char ViewSearchAndEdit();
	std::string ViewAddContact(const short&);
	void ErrorMessages(const int& RecievedError);
	char DisplayOutput(std::string&, std::string&, std::string&);
	~ViewMenuElements();

};
