#pragma once
#include "stdafx.h"
#include "ContactDataModule.h"

ContactDataModule::ContactDataModule(char Optioncatch) : NextOption(Optioncatch)
{
}

ContactDataModule::~ContactDataModule()
{
}	

bool ContactDataModule::FindSearchedData(const char& Selection,const std::string& InputData)
{
	std::string TempString;
	size_t Vector_Count = Found_Mach.empty()? 0:Found_Mach.back();
	((*Option_Ptr == 'n') || (*Option_Ptr == 'N')) ? ++Vector_Count : Vector_Count;
	while(Vector_Count <= (DataVector.size()-1))
	{
		switch (Selection)
		{
		case '1':
		{
			TempString = DataVector[Vector_Count].Name;
			break;
		}
		case '2':
		{
			TempString = DataVector[Vector_Count].Address;
			break;
		}
		case '3':
		{
			TempString = DataVector[Vector_Count].Number;
			break;
		}
		default:break;
		}
		if ((Vector_Count > (DataVector.size() - 1)) && ((NextOption == 'n' || NextOption == 'N') || Found_Mach.empty()))return 0;
		else std::transform(TempString.begin(), TempString.end(), TempString.begin(), tolower);

		if (TempString.find(InputData) != std::string::npos) {
			(Found_Mach.empty() || (Vector_Count != Found_Mach.back())) ? Found_Mach.push_back(Vector_Count) : Found_Mach;
				return 1;
		}else Vector_Count++;
	}
	return 0;
}

bool ContactDataModule::Contact_Manipulation()
{
	switch (NextOption)
	{
	case 'n':
	case 'N':return 1;

	case 'b':
	case 'B':
	{
		if (Found_Mach.size() > 1) {
			Found_Mach.pop_back();
			return 1;
		}
		else return 0;
	}
	case 'e':
	case 'E':
	{
		DataVector.erase(DataVector.begin()+Found_Mach.back());
		Found_Mach.pop_back();
		return 1;
	}
	case 'd':
	case 'D':
	{
		DataVector.erase(DataVector.begin() + Found_Mach.back());
		Found_Mach.pop_back();
		return 1;
	}
	case 'w':
	case 'W':
	case 'q':
	case 'Q':
	{
		Found_Mach.clear();
		return 1;
	}
	}
}

bool ContactDataModule::AddContact(ContactData& ContactLocal)
{
	for (auto i : DataVector)
	{
		if (i.Name.find(ContactLocal.Name) != std::string::npos)return 0;
	}	
	DataVector.push_back(ContactLocal);
	return 1;
}

std::string ContactDataModule::Retrieve_Contact(const short& Info_Retrieve)
{
	switch (Info_Retrieve)
	{
	case 1:return DataVector[Found_Mach.back()].Name;
	case 2:return DataVector[Found_Mach.back()].Number;
	case 3:return DataVector[Found_Mach.back()].Address;
	default: break;
	}
	return "error";
}