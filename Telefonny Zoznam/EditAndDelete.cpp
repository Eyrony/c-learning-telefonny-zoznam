#pragma once
#include "stdafx.h"
#include "EditAndDelete.h"


EditAndDelete::EditAndDelete(char OptionCatch):FileManipulation(OptionCatch)
{
}

EditAndDelete::~EditAndDelete()
{
}

void MenuDecisionCall()
{
	EditAndDelete EditObject('\0');
	bool Quit=false;
	while (!Quit)
	{
		EditObject.ViewInitialMenu();
		char Option=_getch();
		switch (Option)
		{
		case '1': if (EditObject.AddContact(EditObject.DataAcquisition()))
			{
			EditObject.ErrorMessages(4);
			break;
		}
				  else {
					  EditObject.ErrorMessages(5);
					  break;
				  }
		case '2':
		{
			do{
				*EditObject.Option_Ptr = EditObject.ViewSearchAndEdit();
				if (*EditObject.Option_Ptr == 'q')break;
				std::string InputData;
				{
					std::getline(std::cin, InputData);
					std::transform(InputData.begin(), InputData.end(), InputData.begin(), tolower);
				}
				if ( Option == '3' ) EditObject.IsNumber(InputData);
				while (*EditObject.Option_Ptr != 'w' && *EditObject.Option_Ptr != 'W' && *EditObject.Option_Ptr != 'q' && *EditObject.Option_Ptr != 'Q')
				{
					if (EditObject.FindSearchedData(Option, InputData));
					else {
						(EditObject.Size_check()) ? EditObject.ErrorMessages(8) : EditObject.ErrorMessages(9);
					}
					if (!EditObject.Size_check())break;
						*EditObject.Option_Ptr = (EditObject.DisplayOutput( EditObject.Retrieve_Contact(1),
																			EditObject.Retrieve_Contact(2),
																			EditObject.Retrieve_Contact(3)));
					if (EditObject.Contact_Manipulation()) {
						if (*EditObject.Option_Ptr == 'e' || *EditObject.Option_Ptr == 'E') EditObject.AddContact(EditObject.DataAcquisition());
					}
					else EditObject.ErrorMessages(8);
				}
				(*EditObject.Option_Ptr == 'W' || *EditObject.Option_Ptr == 'w') ? *EditObject.Option_Ptr = 'n' : *EditObject.Option_Ptr;
			} while (*EditObject.Option_Ptr != 'q' && *EditObject.Option_Ptr != 'Q');
			break;
		}
		case '0':
		{
			Quit = true;
			break;
		}
		}
	}
}

void EditAndDelete::IsNumber(std::string& InputCheck) {
	if (InputCheck.find_first_not_of("0123456789") != std::string::npos)
	{
		std::cout << "Number entered has non-nummeric characters. Please re-enter:\n";
		std::getline(std::cin, InputCheck);
		IsNumber(InputCheck);
	}
	else return;
	return;
}

ContactData EditAndDelete::DataAcquisition()
{
	ContactData InputData("", "", "");
	InputData.Name = "Name: " + ViewAddContact(0);
	InputData.Number = "Number: " + ViewAddContact(1);
	InputData.Address = "Address: " + ViewAddContact(2);
	return InputData;
}